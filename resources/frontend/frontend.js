import Vue from 'vue';
import router from './router';
import { BootstrapVue } from 'bootstrap-vue';
import Notifications from 'vue-notification';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.use(BootstrapVue);
Vue.use(Notifications);

import Layout from "./components/Layout";

const frontend = new Vue({
    el: '#app',
    methods: {
        showNotify(type, text) {
            let title = 'Успешно';

            if (type === 'error') {
                title = 'Ошибка';
            } else if (type === 'warn') {
                title = 'Инфо';
            }

            this.$notify({
                title: title,
                type,
                text,
                ignoreDuplicates: true
            });
        }
    },
    router,
    components: {
        Layout
    }
});
