import axios from 'axios';

axios.defaults.baseURL = '/api/admin/';

const request = (method, url, data) => {
    return new Promise(async (res, rej) => {
        try {
            let result;

            if (method === 'POST') {
                result = await axios.post(url, data)
            } else {
                result = await axios.get(url, { params: data } )
            }

            return res(result.data);
        } catch (e) {
            if (typeof e.response !== "undefined" && typeof e.response.data.err !== "undefined") {
                return rej(e.response.data.err);
            } else {
                return rej(false);
            }
        }
    });

};

export default class Request {
    static get(url, data) {
        return request('GET', url, data);
    }

    static post(url, data) {
        return request('POST', url, data);
    }

    static setBearerToken(token) {
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    }
}
