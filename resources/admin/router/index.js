import Vue from 'vue'
import VueRouter from 'vue-router'
import Index from "../pages/Index";
import Login from "../pages/Login";
import Cars from "../pages/cars/Cars";
import Orders from "../pages/orders/Orders";
import Create from "../pages/cars/Create";
import CarEdit from "../pages/cars/CarEdit";
import OrderEdit from "../pages/orders/OrderEdit";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Index',
        meta: {
            authGuard: true
        },
        component: Index
    },
    {
        path: '/cars',
        name: 'Cars',
        meta: {
            authGuard: true
        },
        component: Cars
    },
    {
        path: '/cars/create',
        name: 'CarsCreate',
        meta: {
            authGuard: true
        },
        component: Create
    },
    {
        path: '/cars/:car/edit',
        name: 'CarsEdit',
        meta: {
            authGuard: true
        },
        component: CarEdit
    },
    {
        path: '/orders',
        name: 'Orders',
        meta: {
            authGuard: true
        },
        component: Orders
    },
    {
        path: '/orders/:order/edit',
        name: 'Orders',
        meta: {
            authGuard: true
        },
        component: OrderEdit
    },
    {
        path: '/login',
        name: 'Login',
        component: Login
    },
]

const router = new VueRouter({
    mode: 'history',
    base: 'admin',
    routes
})

router.beforeEach((to, from, next) => {
    const loggedIn = sessionStorage.getItem("token");
    if (to.matched.some(m => m.meta.authGuard) && !loggedIn)
        next({ name: "Login" });
    else next();
});

export default router

