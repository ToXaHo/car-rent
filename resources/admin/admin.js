import Vue from 'vue';
import VueCookie from 'vue-cookie';
import { BootstrapVue, Buttons, TablePlugin, Form } from 'bootstrap-vue';
import Notifications from 'vue-notification';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

Vue.use(VueCookie);
Vue.use(BootstrapVue);
Vue.use(Notifications);

import router from './router';
import Layout from "./components/Layout";
import Request from "./helpers/request";

const app = new Vue({
    data() {
        return {
            user: null
        }
    },
    created() {
        if (sessionStorage.getItem("token")) {
            Request.setBearerToken(sessionStorage.getItem("token"));
        }

        this.getUser();
    },
    methods: {
        getUser() {
            Request.get('getAdmin')
                .then((user) => {
                    this.user = user;
                })
                .catch(() => {
                    sessionStorage.removeItem("token")
                })
        },
        showNotify(type, text) {
            let title = 'Успешно';

            if (type === 'error') {
                title = 'Ошибка';
            } else if (type === 'warn') {
                title = 'Инфо';
            }

            this.$notify({
                title: title,
                type,
                text,
                ignoreDuplicates: true
            });
        }
    },
    el: '#app',
    router,
    components: {
        Layout
    }
})
