<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
    </head>
    <body>
        <div id="app">
            <layout></layout>
        </div>
    </body>
    <script src="{{ mix('js/admin.js') }}?v={{time()}}"></script>
</html>
