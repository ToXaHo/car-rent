Новый заказ на сайте.

ID -> <a href="{{ \Illuminate\Support\Facades\Request::root() }}/admin/orders/{{ $order->id }}/edit">{{ $order->id }}</a><br>
Имя -> {{ $order->customer_username }}<br>
Телефон -> {{ $order->customer_telephone }}<br>
Марка -> {{ $order->item->mark }}<br>
Модель -> {{ $order->item->model }}<br>
Гос. номер -> <a href="{{ \Illuminate\Support\Facades\Request::root() }}/admin/cars/{{ $order->item->number }}/edit">{{ $order->item->number }}</a><br>
Год выпуска -> {{ $order->item->year_of_issure }}<br>
Цвет -> {{ $order->item->color }}<br>
Цена аренды -> {{ $order->item->price_per_day }} р.<br>
Дата от -> {{ $order->from_date }}<br>
Дата до -> {{ $order->to_date }}<br>
Сумма аренды -> {{ $order->total_price }} р.<br>
