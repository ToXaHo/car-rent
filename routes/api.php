<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * Клиентская часть
 */
Route::group(['prefix' => 'cars'], function () {
    Route::get('/', [\App\Http\Controllers\CarsController::class, 'getCars']);
});

Route::group(['prefix' => 'orders'], function () {
    Route::post('/create', [\App\Http\Controllers\OrdersController::class, 'createOrder']);
});

/**
 * Админ часть
 */
Route::group(['prefix' => 'admin'], function () {
    Route::post('/auth', [\App\Http\Controllers\Admin\AuthController::class, 'auth']);

    Route::get('/getAdmin', [\App\Http\Controllers\Admin\AdminController::class, 'getAdmin'])->middleware('auth:api');

    Route::group(['prefix' => 'cars', 'middleware' => 'auth:api'], function () {
        Route::get('/', [\App\Http\Controllers\Admin\CarsController::class, 'getCars']);
        Route::post('/create', [\App\Http\Controllers\Admin\CarsController::class, 'createCar']);
        Route::post('/delete/{number}', [\App\Http\Controllers\Admin\CarsController::class, 'deleteCar']);
        Route::get('/car/{number}', [\App\Http\Controllers\Admin\CarsController::class, 'getCar']);
        Route::post('/edit', [\App\Http\Controllers\Admin\CarsController::class, 'editCar']);
    });

    Route::group(['prefix' => 'orders', 'middleware' => 'auth:api'], function () {
        Route::get('/', [\App\Http\Controllers\Admin\OrdersController::class, 'getOrders']);
        Route::get('/order/{id}', [\App\Http\Controllers\Admin\OrdersController::class, 'getOrder']);
        Route::post('/order/{id}/edit', [\App\Http\Controllers\Admin\OrdersController::class, 'editOrder']);
    });
});
