Тестовое задание. Проект аренды автомобилей.

## Установка

1. Клонируем проект с gitlab:
```
https://gitlab.com/ToXaHo/car-rent.git
```

2. Создать базы данных MySQL, создать файл .env из .env.example и заполнить его

Данные от базы:
```
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=laravel
DB_USERNAME=root
DB_PASSWORD=
```

Данные для отправки почты
```
MAIL_MAILER=smtp
MAIL_HOST=smtp.mailtrap.io
MAIL_PORT=2525
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
MAIL_FROM_ADDRESS=null
```

Почта администратора, куда будут приходить оповещения о новых заказах
```
ADMIN_EMAIL=
```

3. В корне запустить три команды:
```
composer install
npm install
npm run prod
```

4. Запустить миграции
```
php artisan migrate
```

5. Заполнить базу данных фейковыми данными
```
php artisan db:seed
```

6. Запустить проект
```
php artisan serve
```

7. Тестировать проект
```
http://localhost:8000/admin - Доступ к админ панели, данные (admin:password)
http://localhost:8000 - Фронт с каталогом
```
