<?php

namespace Database\Factories;

use App\Models\Car;
use Illuminate\Database\Eloquent\Factories\Factory;

class CarFactory extends Factory
{
    const MARKS = ['BMW', 'Audi', 'Ford', 'Bentley', 'Bugatti', 'Kio'];
    const MODELS = ['420i Coupe', 'Sonata', 'Rio', 'Cobalt', 'Nexia'];
    const COLOR = ['Красный', 'Белый', 'Черный', 'Зеленый'];

    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Car::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'photo_url' => $this->faker->image(public_path('images/cars'),640,480, null, false),
            'mark' => $this->faker->randomElement(self::MARKS),
            'model' => $this->faker->randomElement(self::MODELS),
            'year_of_issue' => $this->faker->year,
            'number' => $this->faker->numberBetween(100000, 999999),
            'color' => $this->faker->randomElement(self::COLOR),
            'box' => $this->faker->boolean ? 'Автомат' : 'Механика',
            'price_per_day' => $this->faker->numberBetween(1000, 9999)
        ];
    }
}
