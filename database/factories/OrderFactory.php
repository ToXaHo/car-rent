<?php

namespace Database\Factories;

use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'total_price' => $this->faker->numberBetween(1500, 5000),
            'customer_username' => $this->faker->userName,
            'customer_telephone' => $this->faker->e164PhoneNumber,
            'from_date' => $this->faker->date(),
            'to_date' => $this->faker->date(),
            'confirmed' => $this->faker->boolean
        ];
    }
}
