<?php

namespace Database\Factories;

use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = OrderItem::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'order_id' => function () {
                return Order::factory()
                    ->create()
                    ->id;
            },
            'mark' => $this->faker->randomElement(CarFactory::MARKS),
            'model' => $this->faker->randomElement(CarFactory::MODELS),
            'year_of_issue' => $this->faker->year,
            'number' => $this->faker->numberBetween(100000, 999999),
            'color' => $this->faker->randomElement(CarFactory::COLOR),
            'price_per_day' => $this->faker->numberBetween(1000, 9999)
        ];
    }
}
