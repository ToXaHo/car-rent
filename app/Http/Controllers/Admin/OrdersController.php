<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ValidationAdmin;
use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class OrdersController extends Controller
{
    const COLUMNS = ['customer_username', 'customer_telephone'];
    const COLUMNS_CAR = ['model', 'number', 'price_per_day'];

    public function getOrders(Request $r)
    {
        $filter = $r->get('filter');
        $orders = Order::query();

        $orders->orderBy('id', $r->get('desc'));

        $orders->whereHas('item', function (Builder $query) use ($filter) {
            $query->where('mark', 'LIKE', '%' . $filter . '%');

            foreach (self::COLUMNS_CAR as $column) {
                $query->orWhere($column, 'LIKE', '%' . $filter . '%');
            }

            foreach (self::COLUMNS as $column) {
                $query->orWhere($column, 'LIKE', '%' . $filter . '%');
            }
        });

        return $orders->with(['item'])->paginate(20);
    }

    public function getOrder($id)
    {
        try {
            $order = Order::query()->with(['item'])->find($id);

            if (!$order) {
                return response()->json([
                    'err' => 'Произошла ошибка при получении заказа'
                ], 500);
            }

            return $order;
        } catch (\Exception $e) {
            return response()->json([
                'err' => 'Произошла ошибка при получении заказа'
            ], 500);
        }
    }

    public function editOrder($id, Request $r)
    {
        try {
            $confirmed = (boolean) $r->get('confirmed');

            $order = Order::query()->with(['item'])->find($id);

            if (!$order) {
                return response()->json([
                    'err' => 'Заказ не найден'
                ], 500);
            }

            $order->update([
                'confirmed' => $confirmed
            ]);

            return true;
        } catch (\Exception $e) {
            return response()->json([
                'err' => $e->getMessage()
            ], 500);
        }
    }
}
