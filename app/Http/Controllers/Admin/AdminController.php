<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin');
    }

    /**
     * Получение активного администратора по Bearer токену
     *
     * @param Request $r
     * @return mixed
     */
    public function getAdmin(Request $r)
    {
        return $r->user();
    }
}
