<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ValidationAdmin;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use \Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    private $_validationAdmin;

    public function __construct(
        ValidationAdmin $validationAdmin
    )
    {
        $this->_validationAdmin = $validationAdmin;
    }

    /**
     * Авторизация администратора
     *
     * @param Request $r
     * @return \Illuminate\Http\JsonResponse
     */
    public function auth(Request $r)
    {
        $validData = Validator::make($r->all(), [
            'username' => 'required',
            'password' => 'required',
        ], [
            'username.required' => 'Введите имя пользователя',
            'password.required' => 'Введите пароль'
        ]);

        if ($validData->fails()) {
            return response()->json([
                'err' => $validData->errors()->first()
            ], 500);
        }

        $admin = $this->_validationAdmin->checkValidPassword(
            $r->get('username'),
            $r->get('password')
        );

        if (!$admin) {
            return response()->json([
                'err' => 'Неверное имя пользователя или пароль'
            ], 500);
        }

        $token = Str::random(60);
        $admin->update([
           'api_token' => $token
        ]);

        return response()->json([
            'token' => $token
        ]);
    }
}
