<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\ValidationAdmin;
use App\Http\Controllers\Controller;
use App\Models\Car;
use Illuminate\Http\Request;

class CarsController extends Controller
{
    const COLUMNS = ['mark', 'model', 'number', 'box', 'price_per_day'];

    private $_validationAdmin;

    public function __construct(
        ValidationAdmin $validationAdmin
    )
    {
        $this->_validationAdmin = $validationAdmin;
    }

    public function getCars(Request $r)
    {
        $cars = Car::query();

        $cars->orderBy('id', $r->get('desc'));

        foreach (self::COLUMNS as $column) {
            $cars->orWhere($column, 'LIKE', '%' . $r->get('filter') . '%');
        }

        return $cars->paginate(20);
    }

    public function createCar(Request $r)
    {
        try {
            $data = json_decode($r->get('data'), true);

            $this->_validationAdmin->checkValidCreateCar(
                $data
            );

            $photo_url = null;

            if ($r->hasFile('photo')) {
                $image = $r->file('photo');
                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/images/cars');
                $image->move($destinationPath, $name);

                $photo_url = $name;
            }

            $data['photo_url'] = $photo_url;

            Car::query()->create($data);

            return true;
        } catch (\Exception $e) {
            return response()->json([
                'err' => $e->getMessage()
            ], 500);
        }
    }

    public function deleteCar($number)
    {
        try {
            Car::query()->where('number', $number)->delete();

            return true;
        } catch (\Exception $e) {
            return response()->json([
                'err' => 'Произошла ошибка при удалении'
            ], 500);
        }
    }

    public function getCar($number)
    {
        try {
            $car = Car::query()->where('number', $number)->first();

            if (!$car) {
                return response()->json([
                    'err' => 'Произошла ошибка при получении автомобиля'
                ], 500);
            }

            return $car;
        } catch (\Exception $e) {
            return response()->json([
                'err' => 'Произошла ошибка при получении автомобиля'
            ], 500);
        }
    }

    public function editCar(Request $r)
    {
        try {
            $data = json_decode($r->get('data'), true);

            $this->_validationAdmin->checkValidCreateCar(
                $data
            );

            $car = Car::query()->where('number', $data['number'])->first();

            if (!$car) {
                return response()->json([
                    'err' => 'Автомобиль не найден'
                ], 500);
            }

            $photo_url = $car->photo_url;

            if ($r->hasFile('photo')) {
                $image = $r->file('photo');
                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/images/cars');
                $image->move($destinationPath, $name);

                $photo_url = $name;
            }

            $data['photo_url'] = $photo_url;

            $car->update($data);

            return true;
        } catch (\Exception $e) {
            return response()->json([
                'err' => $e->getMessage()
            ], 500);
        }
    }
}
