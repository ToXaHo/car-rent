<?php

namespace App\Http\Controllers;

use App\Helpers\ValidationFrontend;
use App\Jobs\SendEmailOrder;
use App\Models\Car;
use App\Models\Order;
use App\Models\OrderItem;
use Carbon\Carbon;
use Illuminate\Http\Request;

class OrdersController extends Controller
{
    private $_validationFrontend;
    private $_carsController;

    public function __construct(
        ValidationFrontend $validationFrontend,
        CarsController $carsController
    )
    {
        $this->_validationFrontend = $validationFrontend;
        $this->_carsController= $carsController;
    }

    public function createOrder(Request $r)
    {
        try {
            $this->_validationFrontend->checkValidCreateOrder($r->all());

            $car = Car::query()->find($r->get('car_id'));

            if (!$car) {
                return response()->json([
                    'err' => 'Произошла ошибка'
                ], 500);
            }

            $checkAvailableDate = $this->_carsController->getAvailableCurrentDate($car->number, $r->get('to_date'));

            if (!$checkAvailableDate) {
                return response()->json([
                    'err' => 'По данной дате машина уже забронирована'
                ], 500);
            }

            if (Carbon::parse($r->get('from_date')) > Carbon::parse($r->get('to_date'))) {
                return response()->json([
                    'err' => 'Произошла ошибка'
                ], 500);
            }

            $difference = Carbon::parse($r->get('from_date'))->diffInDays(Carbon::parse($r->get('to_date')));

            $totalPrice = $car->price_per_day * $difference;

            $order = Order::query()->create([
                'total_price' => $totalPrice,
                'customer_username' => $r->get('username'),
                'customer_telephone' => $r->get('telephone'),
                'from_date' => $r->get('from_date'),
                'to_date' => $r->get('to_date')
            ]);

            OrderItem::query()->create([
                'order_id' => $order->id,
                'mark' => $car->mark,
                'model' => $car->model,
                'year_of_issue' => $car->year_of_issue,
                'number' => $car->number,
                'color' => $car->color,
                'price_per_day' => $car->price_per_day
            ]);

            $this->dispatch(
                new SendEmailOrder(Order::query()->with(['item'])->find($order->id))
            );

            return true;
        } catch (\Exception $e) {
            return response()->json([
                'err' => $e->getMessage()
            ], 500);
        }
    }
}
