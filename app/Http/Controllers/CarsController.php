<?php

namespace App\Http\Controllers;

use App\Filters\CarsFilters;
use App\Models\Car;
use App\Models\OrderItem;
use Illuminate\Database\Eloquent\Builder;

class CarsController extends Controller
{
    public function getCars(CarsFilters $carsFilters)
    {
        $filters = $this->getFiltersCars();

        return [
            'items' => Car::filter($carsFilters)->paginate(20),
            'filters' => $filters
        ];
    }

    public function getAvailableCurrentDate($carNumber, $toDate)
    {
        $orderItem = OrderItem::query()
            ->whereHas('order', function (Builder $query) use ($toDate) {
                $query->where('confirmed', 1);
                $query->where('to_date', '>=', $toDate);
            })
            ->with(['order'])
            ->where('number', $carNumber)
            ->first();

        if ($orderItem) {
            return 0;
        } else {
            return 1;
        }
    }

    private function getFiltersCars(): array
    {
        $filters = [];

        $filters['marks'] = Car::query()->select('mark')->distinct()->get()->makeHidden(['available', 'actual_order_date']);
        $filters['models'] = Car::query()->select('model', 'mark')->distinct()->get()->makeHidden(['available', 'actual_order_date']);
        $filters['min_price'] = Car::query()->min('price_per_day');
        $filters['max_price'] = Car::query()->max('price_per_day');
        $filters['min_year'] = Car::query()->min('year_of_issue');
        $filters['max_year'] = Car::query()->max('year_of_issue');
        $filters['boxes'] = Car::query()->select('box')->distinct()->get()->makeHidden(['available', 'actual_order_date']);
        $filters['colors'] = Car::query()->select('color')->distinct()->get()->makeHidden(['available', 'actual_order_date']);

        return $filters;
    }
}
