<?php

namespace App\Helpers;

use App\Models\Admin;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

/**
 * Класс используется для валидации полей для разных методов в админ панели
 */

class ValidationAdmin
{
    /**
     * Проверка валидации логина и пароля администратора
     *
     * @param string $username
     * @param string $password
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|object|bool
     */
    public function checkValidPassword(
        string $username,
        string $password
    ) {
        $admin = Admin::query()->where([['username', $username]])->first();

        if ($admin) {
            if (Hash::check($password, $admin->password)) {
                return $admin;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * Проверка валидации для создания автомобиля
     *
     * @param mixed $data
     * @return bool
     */
    public function checkValidCreateCar(
        $data
    ) {
        $validData = Validator::make($data, [
            'mark' => 'required|string',
            'model' => 'required|string',
            'year_of_issue' => 'required|integer',
            'number' => 'required|integer',
            'color' => 'required|string',
            'box' => 'required|string',
            'price_per_day' => 'required|integer'
        ], [
            'mark.required' => 'Введите марку',
            'model.required' => 'Введите модель',
            'year_of_issue.required' => 'Введите год выпуска',
            'number.required' => 'Введите гос. номер',
            'color.required' => 'Введите цвет',
            'box.required' => 'Введите коробку передач',
            'price_per_day.required' => 'Введите стоимость в сутки',
            'mark.string' => 'Марка должна быть строкой',
            'model.string' => 'Модель должна быть строкой',
            'year_of_issue.integer' => 'Год выпуска должен быть цифрой',
            'number.integer' => 'Гос. номер должен быть цифрой',
            'color.string' => 'Цвет должен быть строкой',
            'box.string' => 'Коробка передач должна быть строкой',
            'price_per_day.integer' => 'Стоимость в сутки должна быть цифрой',
        ]);

        if ($validData->fails()) {
            abort(500, $validData->errors()->first());
        }

        return true;
    }
}
