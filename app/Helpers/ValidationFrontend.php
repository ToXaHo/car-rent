<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Validator;

/**
 * Класс используется для валидации полей для разных методов на фронте
 */

class ValidationFrontend
{

    /**
     * Проверка валидации для создания заказа
     *
     * @param array $data
     */
    public function checkValidCreateOrder(
        array $data
    ) {
        $validData = Validator::make($data, [
            'username' => 'required|string',
            'telephone' => 'required|string',
            'from_date' => 'required|date',
            'to_date' => 'required|date'
        ], [
            'username.required' => 'Введите имя',
            'telephone.required' => 'Введите телефон',
            'from_date.required' => 'Введите дату старта',
            'to_date.required' => 'Введите дату окончания',
            'username.string' => 'Введите корректное имя',
            'telephone.string' => 'Введите корректный телефон',
            'from_date.date' => 'Введите корректную дату старта',
            'to_date.date' => 'Введите корректную дату окончания',
        ]);

        if ($validData->fails()) {
            abort(500, $validData->errors()->first());
        }

        return true;
    }
}
