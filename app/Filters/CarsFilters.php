<?php

namespace App\Filters;

class CarsFilters extends QueryFilter
{
    public function mark($marks)
    {
        return $this->builder->whereIn('mark', $marks);
    }

    public function model($models)
    {
        $filterMarks = [];
        $filterModels = [];

        foreach ($models as $model) {
            $model = json_decode($model);

            $filterMarks[] = $model->mark;
            $filterModels[] = $model->model;
        }

        return $this->builder->whereIn('model', $filterModels)
            ->whereIn('mark', $filterMarks);
    }

    public function box($boxes)
    {
        return $this->builder->whereIn('box', $boxes);
    }

    public function color($colors)
    {
        return $this->builder->whereIn('color', $colors);
    }

    public function minYear($minYear)
    {
        return $this->builder->where('year_of_issue', '>=', $minYear);
    }

    public function maxYear($maxYear)
    {
        return $this->builder->where('year_of_issue', '<=', $maxYear);
    }

    public function minPrice($minPrice)
    {
        return $this->builder->where('price_per_day', '>=', $minPrice);
    }

    public function maxPrice($maxPrice)
    {
        return $this->builder->where('price_per_day', '<=', $maxPrice);
    }
}
