<?php

namespace App\Models;

use App\Filters\QueryFilter;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    use HasFactory;

    protected $fillable = [
        'photo_url', 'mark', 'model', 'year_of_issue', 'number', 'color', 'box', 'price_per_day'
    ];

    protected $appends = [
        'available', 'actual_order_date'
    ];

    /**
     * Фильтрация полей по адресной строке
     *
     * @param Builder $builder
     * @param QueryFilter $filters
     * @return Builder
     */
    public function scopeFilter(Builder $builder, QueryFilter $filters)
    {
        return $filters->apply($builder);
    }

    /**
     * Получить актуальный статус брони автомобиля
     *
     * @return boolean
     */
    public function getAvailableAttribute()
    {
        $orderItem = OrderItem::query()
            ->whereHas('order', function (Builder $query) {
                $query->where('confirmed', 1);
                $query->where('to_date', '>=', Carbon::now());
            })
            ->with(['order'])
            ->where('number', $this->number)
            ->first();

        if ($orderItem) {
            return 0;
        } else {
            return 1;
        }
    }

    /**
     * Получить актуальную дату возможной аренды автомобиля
     *
     * @return boolean
     */
    public function getActualOrderDateAttribute()
    {
        $orderItem = OrderItem::query()
            ->whereHas('order', function (Builder $query) {
                $query->where('confirmed', 1);
                $query->where('to_date', '>=', Carbon::now());
                $query->orderBy('to_date', 'DESC');
            })
            ->with(['order'])
            ->where('number', $this->number)
            ->first();

        if ($orderItem) {
            return Carbon::parse($orderItem->order->to_date)->addDay()->format('Y-m-d');
        } else {
            return Carbon::now()->format('Y-m-d');
        }
    }
}
