<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'total_price', 'customer_username', 'customer_telephone', 'from_date', 'to_date', 'confirmed'
    ];

    public function item()
    {
        return $this->hasOne('App\Models\OrderItem', 'order_id', 'id');
    }
}
